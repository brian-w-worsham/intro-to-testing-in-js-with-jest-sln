const cloneArray = require("./cloneArray");

test("properly clones array", () => {
    const array = [1, 2, 3];
    expect(cloneArray(array)).toEqual(array);// toEqual chcks if the value of the objects are the same rather than if they are the same object referenced in memory
    expect(cloneArray(array)).not.toBe(array);//toBe checks if the item is the same object referencerd in memory
});